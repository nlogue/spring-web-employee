package com.citi.training.employees.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MysqlEmployeeDaoTests {
	
	@Autowired
	MysqlEmployeeDao mysqlEmployeeDao;
	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlEmployeeDao.create(new Employee(-1, "Niall", 10.0));
		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}
	
	@Test
	@Transactional
	public void test_findByID() {
		mysqlEmployeeDao.create(new Employee(10, "Niall", 10.0));
		mysqlEmployeeDao.create(new Employee(45, "Thomas", 10.0));
		assertEquals(mysqlEmployeeDao.findById(3).getId(), 3);
	}
	@Test (expected = EmployeeNotFoundException.class)
	@Transactional
	public void test_employeeNotFoundByID() {
		mysqlEmployeeDao.create(new Employee(10, "Niall", 10.0));
		mysqlEmployeeDao.findById(10);
	}
	
	@Test
	@Transactional
	public void test_deleteByID() {
		mysqlEmployeeDao.create(new Employee(1, "Niall", 10.0));
		assertEquals(mysqlEmployeeDao.deleteById(1), 1);
	}
	@Test (expected = EmployeeNotFoundException.class)
	@Transactional
	public void test_deleteByIDException() {
		assertEquals(mysqlEmployeeDao.deleteById(1), 0);
	}

}
