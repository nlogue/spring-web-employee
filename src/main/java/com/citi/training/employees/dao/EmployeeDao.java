package com.citi.training.employees.dao;

import java.util.List;

import com.citi.training.employees.model.Employee;

public interface EmployeeDao {

    List<Employee> findAll();

    Employee findById(int id);

    Employee create(Employee employee);

    int deleteById(int id);
}
