package com.citi.training.employees.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;


@Component
//@Profile ("mysql-dao")
public class MysqlEmployeeDao implements EmployeeDao {
	
	@Autowired
	JdbcTemplate tpl;
	

	@Override
	public List<Employee> findAll() {
		return tpl.query("SELECT id, name, salary FROM employees", new EmployeeMapper());
	}
	
	private static final class EmployeeMapper implements RowMapper<Employee>{
		@Override
		public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			return new Employee(rs.getInt("id"), rs.getString("name"), rs.getDouble("salary"));
		}
	}

	@Override
	public Employee findById(int id) {
		List<Employee> employees = tpl.query(
											"SELECT id, name, salary FROM employees WHERE id = ?", 
											new Object[] {id},
											new EmployeeMapper());
		if (employees.size() <=0) {
			throw new EmployeeNotFoundException("No Employee Found");
		}
		return employees.get(0);
	}

	@Override
	public Employee create(Employee employee) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		if(tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO employees (name, salary) VALUES(?,?)",
				Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, employee.getName());
				preparedStatement.setDouble(2, employee.getSalary());
				return preparedStatement;
			}
		}, keyHolder) == 0) {
			throw new EmployeeNotFoundException("Could not create Employee");
		}else {
		employee.setId(keyHolder.getKey().intValue());
		return employee;
		}
	}

	@Override
	public int deleteById(int id) {
		int rowsAffected= tpl.update("DELETE FROM employees WHERE id = ?", id);
		if(rowsAffected == 0){
			throw new EmployeeNotFoundException("No Employees Deleted");
		}else {
			return rowsAffected;
		}
		
		
	}

}
