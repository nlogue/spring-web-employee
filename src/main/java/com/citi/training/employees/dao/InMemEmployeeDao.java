package com.citi.training.employees.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

@Component
@Profile("inmem-dao")
public class InMemEmployeeDao implements EmployeeDao {

    private static AtomicInteger idGenerator = new AtomicInteger();

    private Map<Integer, Employee> allEmployees = new HashMap<Integer, Employee>();

    @Override
    public Employee create(Employee employee) {
        employee.setId(idGenerator.addAndGet(1));
        allEmployees.put(employee.getId(), employee);
        return employee;
    }

    @Override
    public Employee findById(int id) {
        Employee employee = allEmployees.get(id);
        if (employee == null) {
            throw new EmployeeNotFoundException("Employee has not been found");
        }
        return employee;
    }

    @Override
    public List<Employee> findAll() {
        return new ArrayList<Employee>(allEmployees.values());
    }

    @Override
    public int deleteById(int id) {
        Employee employee = allEmployees.remove(id);
        if (employee == null) {
            throw new EmployeeNotFoundException("Employee could not be deleted");
        }else {
        	return employee.getId();
        }
    }

}
